package com.example.finaluri

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        auth = FirebaseAuth.getInstance()
        init()
        RegistrationButton.setOnClickListener {
            startActivity(Intent(this, ActivityReg::class.java))
        }
    }

    private fun init() {
        readData()
        LogInButton.setOnClickListener {
            saveUserData()
            intent.putExtra("email",EmailEditText.text.toString())
            intent.putExtra("pass",PasswordEditText.text.toString())
        }
        LogInButton.setOnClickListener {
            auth.signInWithEmailAndPassword(
                EmailEditText.text.toString(),
                PasswordEditText.text.toString()
            )
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        Toast.makeText(this, "success", Toast.LENGTH_LONG).show()
                        startActivity(Intent(this, ActivityMain2::class.java))
                    } else {
                        Toast.makeText(this, "incorrect", Toast.LENGTH_LONG).show()
                    }
                }

        }
    }
    private fun saveUserData(){
        val sharedPreferences = getSharedPreferences("user_Data",Context.MODE_PRIVATE)
        val edit = sharedPreferences.edit()
        edit.putString("email",EmailEditText.text.toString())
        edit.putString("pass",PasswordEditText.text.toString())
    }
    private fun readData(){
        val sharedPreferences = getSharedPreferences("user_Data", Context.MODE_PRIVATE)
        EmailEditText.setText(sharedPreferences.getString("email",""))
        PasswordEditText.setText(sharedPreferences.getString("pass",""))
    }


}

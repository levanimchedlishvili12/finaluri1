package com.example.finaluri

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main2.*

class ActivityMain2 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        TransportButton.setOnClickListener {
            startActivity(Intent(this, TransportsActivity::class.java))
        }
        HistoryButton.setOnClickListener {
            startActivity(Intent(this, ActivityHistory::class.java))
        }
        HotelButton.setOnClickListener {
            startActivity(Intent(this, HotelActivity2::class.java))
        }
        LocationButton.setOnClickListener {
            startActivity(Intent(this, LocationActivity::class.java))
        }
        MuseumButton.setOnClickListener {
            startActivity(Intent(this, ActivityMuseum::class.java))

        }

    }
}

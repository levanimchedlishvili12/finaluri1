package com.example.finaluri

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_transports.*

class TransportsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transports)
        TaxiButton.setOnClickListener {
            startActivity(Intent(this, ActivityTaxi::class.java))
        }
        MetroButton.setOnClickListener {
            startActivity(Intent(this, ActivityMetro::class.java))
        }
    }
}

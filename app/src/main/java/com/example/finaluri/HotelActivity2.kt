package com.example.finaluri

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_hotel2.*

class HotelActivity2 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hotel2)
        FiveStarButton.setOnClickListener {
            startActivity(Intent(this, HotelActivity::class.java))
        }
        FourStarButton.setOnClickListener {
            startActivity(Intent(this, FourstarhotelActivity::class.java))
        }
        ThreeStarButton.setOnClickListener {
            startActivity(Intent(this, ThreestarHotelActivity::class.java))
        }
    }
}
